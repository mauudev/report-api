import openSocket from 'socket.io-client';

const socket = openSocket('http://localhost:4000');

function subscribeToTimer (cb) {
    socket.on('timer', timestamp => cb(timestamp));
    socket.emit('subscribeToTimer', 1000);
}

// function updateKills (cb) {
//     socket.on('')
//     socket.emit('updateKills', {//enviamos estos datos al servidor
//         kills1: kills.value,
//     });
// }

export {
    subscribeToTimer,
    // updateKills,
}