const Score = require('../models/score.model');
const Player = require('../models/player.model');

//https://stackoverflow.com/questions/39807409/updating-multiple-mongodb-collections-from-node-js
// exports.create = async(req, res) => {
//     if(!req.body.kills || !req.body.deaths){
//         return res.status(500).send({
//             message: 'Score fields can not be empty !'
//         });
//     }
//     const {kills, deaths, player} = req.body;
//     const score = new Score({kills, deaths, player});
//     await score.save();
//     res.send(score);
// };

exports.create = async(req, res) => {
    if(!req.body.kill || !req.body.dead){
        return res.status(500).send({
            message: 'Score fields can not be empty !'
        });
    }

    const {kill, dead} = req.body;
    
    let killerScore = await Score.findOne({player: kill});
    let deadScore = await Score.findOne({player: dead});

    const queryKiller = {_id: killerScore._id};
    const queryDead = {_id: deadScore._id};
    
    try {
        await Score.findOneAndUpdate(queryKiller,
            { $inc:{ kills: 1 }},{new :true}); 
    } catch (error) {
        console.log('Error updating !');    
    }
    
    try {
        const target = Score.findOne({_id: queryDead._id});
        if(target.deaths == 0){
            await Score.findOneAndUpdate(queryDead,
                { deaths: 0},{new :true});
        }else{
            await Score.findOneAndUpdate(queryDead,
                { $inc:{deaths: 1} },{new :true});
        }
    } catch (error) {
        console.log('Error updating !');
    }
    
    res.redirect('../main');
};

exports.findAll = (req, res) => {
    Score.find().then(scores => {
        res.send(scores);
    }).catch(err => {
        res.status(500).send({
            message: err.message || 'An error occurred while data retrieving !'
        });
    });
};

exports.findOne = (req, res) => {
    Score.findById(req.params.scoreId).then(score => {
        if(!score){
            return res.status(404).send({
                message: 'Score with id: '+req.params.scoreId+' not found !'
            });
        }
        res.send(score);
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound'){
            return res.status(404).send({
                message: err.message || 'Score with id: '+req.params.scoreId+' not found !'
            });
        }
        return res.status(404).send({
            message: err.message || 'Score with id: '+req.params.scoreId+' not found !'
        });
    });
};

exports.update = (req, res) => {
    if(!req.body.kills || !req.body.deaths){
        return res.status(500).send({
            message: 'Error ! Score fields can not be empty.'
        });
    }
    Score.findByIdAndUpdate(req.params.scoreId, {
        kills: req.body.kills,
        deaths: req.body.deaths,
        player: req.body.player
    },{new: true}).then(score => {
        if(!score){
            return res.status(404).send({
                message: err.message || 'Score with id: '+req.params.scoreId+' not found !'
            });
        }
        res.send(score);
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound'){
            return res.status(404).send({
                message: err.message || 'Score with id: '+req.params.scoreId+' not found !'
            });
        }
        return res.status(500).send({
            message: err.message || 'Score with id: '+req.params.scoreId+' not found !'
        });
    });
};

exports.delete = (req, res) => {
    Score.findByIdAndRemove(req.params.scoreId)
    .then(score => {
        if(!score){
            return res.status(404).send({
                message: err.message || 'Score with id: '+req.params.scoreId+' not found !'
            });
        }
        res.send({
            message: 'Score data deleted successfully !'
        });
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound'){
            return res.status(404).send({
                message: err.message || 'Score with id: '+req.params.scoreId+' not found !'
            });
        }
        return res.status(500).send({
            message: err.message || 'Score with id: '+req.params.scoreId+' not found !'
        });
    });
};