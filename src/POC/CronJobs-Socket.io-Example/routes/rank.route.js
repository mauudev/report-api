module.exports = (app) => {
    const rank = require('../controllers/rank.controller.js');

    // Create a new rank
    app.post('/rank/create', rank.create);

    // Retrieve all rank
    app.get('/rank', rank.findAll);

    // Retrieve a single rank with rankId
    app.get('/rank/:rankId/find', rank.findOne);

    // Update a rank with rankId
    app.put('/rank/:rankId/update', rank.update);

    // Delete a rank with rankId
    app.delete('/rank/:rankId/delete', rank.delete);
}