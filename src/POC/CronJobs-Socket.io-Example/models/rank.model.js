const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const RankSchema = mongoose.Schema({
    rank: Number,
    player: { type: Schema.ObjectId, ref: "Player" } 
}, {
    timestamps: true
});

module.exports = mongoose.model('Rank', RankSchema);
