import express from 'express'
import http from 'http'
import socketIO from 'socket.io';
import bodyParser from 'body-parser'

const app = express();
const server = http.createServer(app)
const io = socketIO(server);

const host = "127.0.0.1";
const port = "4000";

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
// const io = require('socket.io')();

app.get('/', (req, res) => {
    res.send('Server is running')
   })
   
app.post('/kills', (req, res) => {
    io.on('connection', (client) => {
        client.on('subscribeToTimer', (interval) => {
            console.log('client is subcribing to timer with interval', interval);
            setInterval(() => {
                client.emit('timer', req.body.name);
            }, interval);
        });
        // client.on('updateKills', (interval) => {
        //     console.log('client is subcribing to timer with interval', interval);
        //     client.emit('kills', "kills player")
        //     // setInterval(() => {
        //     //     client.emit('timer', new Date());
        //     // }, interval);
        // });
    });
    res.send('Server is running');
})



server.listen(port, host, () => {
    // console.log(`APP: ${get('app')}`)
    console.log(`Server Created, Ready to listen at ${host}:${port}`)
   })
// const port = 8000;
// io.listen(8000);
// console.log('listening on port', port);