import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const PlayerSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    nick: {
        type: String,
        required: true
    },
    score: {
        type: Number,
        required: true
    },
    kills: {
        type: Number,
        required: true
    },
    deaths: {
        type: Number,
        required: true
    }
});
export default mongoose.model('Player', PlayerSchema);