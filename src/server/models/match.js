import mongoose from 'mongoose';
import team from './team'
const Schema = mongoose.Schema;

const MatchSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    matchDate: {
        type: Date,
        required: true
    },
    totalScore: {
        type: Number,
        required: true
    },
    duration: {
        type: Time,
        required: true
    },
    teams: [team]
});
export default mongoose.model('Team', TeamSchema);