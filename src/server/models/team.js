import mongoose from 'mongoose';
import player from './player'
const Schema = mongoose.Schema;

const TeamSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    players: [player]
});
export default mongoose.model('Team', TeamSchema);